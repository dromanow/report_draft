from datetime import date, timedelta
import psycopg2


def get_query(cursor):  # decode query text from cursor
   return cursor.query.decode('cp1251')


def get_first_day(dt, d_years=0, d_months=0):  # for month
   y, m = dt.year + d_years, dt.month + d_months
   a, m = divmod(m - 1, 12)
   return date(dt.year + a, m + 1, 1)


def get_last_day(dt):  # for month
   return get_first_day(dt, 0, 1) + timedelta(-1)


def get_sql(name):  # load sql from file
   return open(name + '.sql', 'r').read().encode('cp1251').decode('utf8')


def get_rc_connection():
   conn = psycopg2.connect(database="inside.tensor.ru", user="postgres", password="postgres",
                           host='rc-inside-db2.unix.tensor.ru')
   cur = conn.cursor()
   cur.execute('set local search_path=\'_00000003\';')
   return conn, cur


def get_local_connection():
   conn = psycopg2.connect(database="БД inside.tensor.ru", user="postgres", password="postgres",
                           host='localhost')
   cur = conn.cursor()
   cur.execute('set local search_path=\'public\';')
   return conn, cur
