--explain(analyze, buffers)
SELECT DISTINCT
   "Работа"."ТипДокумента"
   , SUM("Работа"."ВремяКнц"::time - "Работа"."ВремяНач"::time)
   , COUNT(*)
   , "ТипДокумента"."ТипДокумента"
FROM
   "Работа"
LEFT OUTER JOIN "ТипДокумента" ON "ТипДокумента"."@ТипДокумента" = "Работа"."ТипДокумента"
WHERE
   "Работа"."Дата" >= %(start_date)s
   AND "Работа"."Дата" <= %(end_date)s
   AND "Работа"."ЧастноеЛицо" = ANY(SELECT "Лицо" FROM person_list)
   AND COALESCE("Работа"."Тип", 0) IN (0, 15)
   AND "Работа"."Удалена" IS NOT TRUE

GROUP BY
   "Работа"."ТипДокумента"
   , "ТипДокумента"."@ТипДокумента"
;
