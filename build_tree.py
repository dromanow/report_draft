from datetime import date
from tools import get_rc_connection, get_local_connection, get_last_day, get_first_day, get_sql

local_conn, local_cur = get_local_connection()

conn, cur = get_rc_connection()


class TreeNode:
   TYPE_TASK_LIST = 1
   TYPE_PROJECT_LIST = 2
   TYPE_ABSENT_LIST = 3
   TYPE_OTHER_DOCS_LIST = 4
   TYPE_UNLINKED_LIST = 5
   TYPE_PROJECT = 11
   TYPE_STAGE = 12
   TYPE_TASK = 13
   TYPE_REG = 14
   TYPE_VACATION = 15
   TYPE_REST = 16
   TYPE_ILL = 17
   TYPE_OTHER = 18

   ROOT_TYPES = (TYPE_TASK_LIST, TYPE_PROJECT_LIST, TYPE_ABSENT_LIST, TYPE_UNLINKED_LIST, TYPE_OTHER_DOCS_LIST)

   def __init__(self, node_id, parent, doc_id, node_type, reg_id):
      super().__init__()
      self.id = node_id
      self._parent = parent
      self.doc_id = doc_id
      self.type = node_type
      self.reg_id = reg_id
      self.works = []

   def print_up(self):
      info = str(self)
      if self._parent:
         return self._parent.print_up() + ' <- ' + info
      return info

   def __str__(self):
      return '{0}{1}({2})'.format(self.__class__.__name__, self.doc_id if self.doc_id else '', len(self.works))

   @property
   def parent_id(self):
      if self._parent:
         return self._parent.id

   @property
   def parent(self):
      return self._parent

   def get_parents(self):
      if self._parent:
         return self._parent.get_parents() + [self.id]
      return [self.id]

   def add_work(self, work):
      parents = self.get_parents()

      q = '''
         SELECT EXISTS(SELECT 1 FROM "ОтчетВремяЛицо" WHERE "Лицо" = %s AND "Узел" = %s AND "Дата" = %s)
      '''
      local_cur.execute(q, (work.person, self.id, get_first_day(work.date)))
      if local_cur.fetchone()[0]:
         q = '''
            UPDATE "ОтчетВремяЛицо"
            SET "Время" = "Время" + %(time)s, "Количество" = "Количество" + 1
            WHERE "Лицо" = %(person)s AND "Узел" = %(node)s
            ;
         '''
         local_cur.execute(q, {'person': work.person, 'node': self.id, 'date': get_first_day(work.date),
                               'time': work.time})
         local_conn.commit()
      else:
         q = '''
            INSERT INTO "ОтчетВремяЛицо" ("Лицо", "Узел", "Дата", "Время", "Количество","Родители")
            VALUES (%s, %s, %s, %s, 1, %s)
            ;
         '''
         local_cur.execute(q, (work.person, self.id, get_first_day(work.date), work.time, parents))
         local_conn.commit()

   def _exists(self):
      if self.id:
         return True

      if self.doc_id:
         cond = '"ОтчетВремяСтруктура"."Документ" = {}'.format(self.doc_id)
      elif self.reg_id and self._parent:
         cond = '''
            "ОтчетВремяСтруктура"."Регламент" = {} AND
            "ОтчетВремяСтруктура"."Родитель" = {}
         '''.format(self.reg_id, self.parent_id)
      elif self.type in TreeNode.ROOT_TYPES:
         cond = '"ОтчетВремяСтруктура"."ТипУзла" = {}'.format(self.type)
      else:
         print("Warning: Undefined node params, data may be lost.")
         return False
      q = '''
         SELECT "@Узел", "Родитель", "Документ", "ТипУзла", "Регламент"
         FROM "ОтчетВремяСтруктура"
         WHERE {};
      '''.format(cond)
      local_cur.execute(q)

      if local_cur.rowcount == 1:
         self.id = local_cur.fetchone()[0]
         return True

      if local_cur.rowcount > 1:
         print("Warning: found > 1 tree nodes")

      return False

   def _create(self):
      if not self.id:
         q = '''
            INSERT INTO "ОтчетВремяСтруктура" ("Родитель", "Документ", "ТипУзла", "Регламент")
            VALUES (%s, %s, %s, %s)
            RETURNING "@Узел";
         '''
         local_cur.execute(q, (self.parent_id, self.doc_id, self.type, self.reg_id))
         self.id = local_cur.fetchone()[0]
         local_conn.commit()

   def update_chain(self):
      if self.id:
         return

      if self._parent:
         self._parent.update_chain()

      if self._exists():
         return

      self._create()

   def set_parent(self, parent):
      self._parent = parent


class Reg(TreeNode):
   def __init__(self, reg_id, parent):
      super().__init__(node_id=None, parent=parent, doc_id=None, node_type=TreeNode.TYPE_REG, reg_id=reg_id)
      self.reg = []

   def append(self, doc):
      self.reg.append(doc)
      doc.set_parent(self)

   def __str__(self):
      return '{0}{1}({2})'.format(self.__class__.__name__, self.reg_id if self.reg_id else '', len(self.works))


class RegHolder:
   def __init__(self):
      super().__init__()
      self.reg = {}

   def add_reg(self, doc, reg_id):
      if reg_id not in self.reg.keys():
         self.reg[reg_id] = Reg(reg_id, self)
      self.reg[reg_id].append(doc)


class OtherDoc(TreeNode):
   def __init__(self, doc_id, obj_type=None):
      super().__init__(node_id=None, doc_id=doc_id, parent=None, node_type=TreeNode.TYPE_OTHER, reg_id=None)
      self.obj_type = obj_type


class RootNode(TreeNode):
   def __init__(self, node_type):
      super().__init__(node_id=None, doc_id=None, parent=None, node_type=node_type, reg_id=None)
      self.objects = []

   def append(self, value):
      value.set_parent(self)
      self.objects.append(value)


class ProjectList(RootNode):
   def __init__(self):
      super().__init__(TreeNode.TYPE_PROJECT_LIST)


class TasksList(RootNode, RegHolder):
   def __init__(self):
      super().__init__(TreeNode.TYPE_TASK_LIST)


class Project(TreeNode, RegHolder):
   def __init__(self, doc_id, node_type=TreeNode.TYPE_PROJECT):
      super().__init__(node_id=None, doc_id=doc_id, parent=None, node_type=node_type, reg_id=None)
      self.stages = []
      self.tasks = []

   def append_child(self, stage):
      self.stages.append(stage)
      stage.set_parent(self)

   def append_task(self, task):
      self.tasks.append(task)
      task.set_parent(self)


class Stage(Project):
   def __init__(self, doc_id):
      super().__init__(doc_id, node_type=TreeNode.TYPE_STAGE)


class Task(TreeNode):
   def __init__(self, doc_id):
      super().__init__(node_id=None, doc_id=doc_id, parent=None, node_type=TreeNode.TYPE_TASK, reg_id=None)
      self.tasks = []

   def append_task(self, task):
      self.tasks.append(task)
      task.set_parent(self)


class TaskTree:
   TASK_TYPE = 15
   PROJECT_TYPE = 2058
   STAGE_TYPE = 2059

   def __init__(self):
      self.projects = ProjectList()
      self.tasks = TasksList()
      self.others_docs = RootNode(TreeNode.TYPE_OTHER_DOCS_LIST)
      self.unlinked_works = RootNode(TreeNode.TYPE_UNLINKED_LIST)

      for n in (self.projects, self.tasks, self.others_docs, self.unlinked_works):
         n.update_chain()

      self.tasks_buffer = {}
      self.stages_buffer = {}
      self.others_docs_buffer = {}

   @staticmethod
   def _task_get_parent_stage(task_id):
      """
      :param task_id: task_id идентификатор документа задачи
      :return: идентификатор документа к которому привязана задача task_id
      """

      q = '''
         WITH RECURSIVE task_branch AS (
            SELECT "Документ"."@Документ", "РазличныеДокументы"."БазовыйДокумент"
            FROM "Документ"
            JOIN "РазличныеДокументы" ON "РазличныеДокументы"."@Документ" = "Документ"."@Документ"
            WHERE "Документ"."@Документ" = %(doc_id)s

            UNION

            SELECT "Документ"."@Документ", "РазличныеДокументы"."БазовыйДокумент"
            FROM "Документ"
            JOIN "РазличныеДокументы" ON "РазличныеДокументы"."@Документ" = "Документ"."@Документ"
            JOIN task_branch ON "Документ"."@Документ" = task_branch."БазовыйДокумент"
            )

            SELECT "СвязьДокументов"."ДокументОснование"
            FROM "СвязьДокументов"
            JOIN "Документ" ON "СвязьДокументов"."ДокументОснование" = "Документ"."@Документ"
            WHERE
               "ДокументСледствие" = ANY(SELECT "@Документ" FROM task_branch) AND
               "Документ"."ТипДокумента" IN %(parent_types)s AND
               "Документ"."Удален" = FALSE
      '''

      cur.execute(q, {'doc_id': task_id, 'parent_types': (TaskTree.PROJECT_TYPE, TaskTree.STAGE_TYPE)})
      return [i for i in cur.fetchall()]

   def _build_stage_branch(self, stage_id):
      """
      Строит цепочку этапов/проектов до корня. Если какие либо из зтапов цепочки уже существуют привязывает новые
      элементы в качетсве дочерней цепочки.
      :param stage_id: идентификатор документа (этапа/проекта) с которого начинается построение
      :return: объект соотвесвующий stage_id
      """

      if not stage_id:
         return

      q = '''
         WITH RECURSIVE stage_branch AS (
            SELECT
               "Документ"."@Документ",
               "Документ"."Раздел",
               "Документ"."ТипДокумента",
               "Документ"."Раздел@",
               0 as level
            FROM "Документ"
            WHERE "Документ"."@Документ" = %(stage_id)s

            UNION

            SELECT
               "Документ"."@Документ",
               "Документ"."Раздел",
               "Документ"."ТипДокумента",
               "Документ"."Раздел@",
               stage_branch.level + 1 as level
            FROM "Документ"
            JOIN stage_branch ON "Документ"."@Документ" = stage_branch."Раздел"
         )

         SELECT
            stage_branch."@Документ",
            stage_branch."ТипДокумента"
         FROM stage_branch
         WHERE stage_branch."Раздел@" IS NULL
         ORDER BY level
      '''
      cur.execute(q, {'stage_id': stage_id})
      stage = None
      child = None
      head = None
      new = False

      for e in cur.fetchall():
         doc_id, doc_type = e
         new = False
         if doc_id not in self.stages_buffer.keys():
            if doc_type == TaskTree.PROJECT_TYPE:
               self.stages_buffer[doc_id] = Project(doc_id)
            elif doc_type == TaskTree.STAGE_TYPE:
               self.stages_buffer[doc_id] = Stage(doc_id)
            else:
               # Без паники. Это нормально, у нас есть задачи привязаные к совещаниям которые относяться в висячим
               # задачам
               # print("Error: Undefined type in stage tree ({}:{})".format(doc_id, doc_type))
               return
            new = True
         head = self.stages_buffer[doc_id]

         if not stage:
            stage = head
         if child:
            child.set_parent(head)
         child = head

         if not new:
            break

      if head and new:
         self.projects.append(head)

      return stage

   def _get_task(self, doc_id, reg):

      def get_task(stage_doc):
         key = (doc_id, reg, stage_doc)
         if key not in self.tasks_buffer.keys():
            self.tasks_buffer[key] = Task(doc_id)
         return self.tasks_buffer[key]

      parents_id = self._task_get_parent_stage(doc_id)

      tasks = []
      if parents_id:
         for stage_id in parents_id:
            parent = self._build_stage_branch(stage_id)
            if parent:
               task = get_task(stage_id)
               parent.add_reg(task, reg)
            else:
               task = get_task(None)
               self.tasks.add_reg(task, reg)
            tasks.append(task)
      else:
         tasks = [get_task(None)]
         self.tasks.add_reg(tasks[0], reg)

      return tasks

   def _get_other_doc(self, doc_id, doc_type):
      if doc_id not in self.others_docs_buffer.keys():
         other = OtherDoc(doc_id, doc_type)
         self.others_docs_buffer[doc_id] = other
         self.others_docs.append(other)
         return other
      return self.others_docs_buffer[doc_id]

   @staticmethod
   def update_chain(node: TreeNode):

      def get_branch(n: TreeNode):
         if not n.parent:
            return [n]
         return get_branch(n.parent) + [n]
      branch = get_branch(node)
      stage = next((f for f in reversed(branch) if f.type in (TreeNode.TYPE_PROJECT, TreeNode.TYPE_STAGE)),
                   None)
      if stage:
         q = '''
            WITH RECURSIVE nodes_branch AS (
               SELECT "@Узел", "Родитель", "ТипУзла", "Документ", "Регламент"
               FROM "ОтчетВремяСтруктура"
               WHERE "Документ" = %(doc_id)s

               UNION

               SELECT
                  "ОтчетВремяСтруктура"."@Узел",
                  "ОтчетВремяСтруктура"."Родитель",
                  "ОтчетВремяСтруктура"."ТипУзла",
                  "ОтчетВремяСтруктура"."Документ",
                  "ОтчетВремяСтруктура"."Регламент"
               FROM "ОтчетВремяСтруктура"
               JOIN nodes_branch ON "ОтчетВремяСтруктура"."@Узел" = nodes_branch."Родитель"
            )
            SELECT * FROM nodes_branch
         '''
         local_cur.execute(q, {'doc_id': stage.doc_id})
         # Собрали данные узлов из базы и в памяти (branch) и пытаемся их сопоставить
         # Основная идея в том что может быть одинаковое начало, но разные продолжения из-за переноса этапа в другие
         # проекты/этапы

         for b, data in zip(branch, cur.fetchall()):
            # Если не совспали типы сразу различие
            if b.type != data[2]:
               break

            # Корневой узел может быть только один, по этому мы автоматом присваемваем ему id из БД
            if b.type in TreeNode.ROOT_TYPES:
               b.id = data[0]

            # Для этапа и проекта должны совпадать id документы
            elif b.type in (TreeNode.TYPE_PROJECT, TreeNode.TYPE_STAGE):
               if b.doc_id == data[3]:
                  b.id = data[0]
               else:
                  break

            # Для регламента должна совпадать пара id регламента и id родителя, т.к. один регламент может быть привязан
            # к разным этапам/проектам. Еще т.к. регламенты это листья дерева, то к этом моменту уже определен id
            # на предыдущем этапе.
            elif b.type == TreeNode.TYPE_REG:
               if b.reg == data[4] and b.parent_id == data[1]:
                  b.id = data[0]

            # Что то не понятное, выходим
            else:
               break

      node.update_chain()

   @staticmethod
   def print_info(obj):
      print(obj.print_up())

   def add_work(self, work):
      if not work.doc_type:
         docs = [self.unlinked_works]
      elif work.doc_type == self.STAGE_TYPE or work.doc_type == self.PROJECT_TYPE:
         docs = [self._build_stage_branch(work.doc)]
      elif work.doc_type == self.TASK_TYPE:
         docs = self._get_task(work.doc, work.reg)
      else:
         docs = [self._get_other_doc(work.doc, work.doc_type)]

      for doc in docs:
         self.update_chain(doc)
         doc.add_work(work)
         # self.print_info(doc)


def process_works():
   class Work:
      def __init__(self, doc, person, time, doc_type, work_date, reg):
         self.doc = doc
         self.person = person
         self.time = time
         self.doc_type = doc_type
         self.date = work_date
         self.reg = reg

   md = date(2015, 12, 1)

   cur.execute(get_sql('get_all_works')
               # + ' AND "Работа"."Документ" = 38662063'
               # + ' LIMIT 100'
               ,
               {'dep_id': 1371, 'start_date': get_first_day(md), 'end_date': get_last_day(md)})

   works = [Work(*w) for w in cur.fetchall()]

   tree = TaskTree()
   print('works loaded')

   for w in works:
      tree.add_work(w)

   print(len(works))


def get_report():
   def get_level(parent_id):
      q = '''
           WITH RECURSIVE nodes_branch AS (
              SELECT "@Узел", "Родитель", "ТипУзла", "Документ", 1 AS count
              FROM "ОтчетВремяСтруктура"
              WHERE "Родитель" {}

              UNION

              SELECT
                  "ОтчетВремяСтруктура"."@Узел",
                  "ОтчетВремяСтруктура"."Родитель",
                  "ОтчетВремяСтруктура"."ТипУзла",
                  "ОтчетВремяСтруктура"."Документ",
                  nodes_branch.count + 1 AS count
              FROM "ОтчетВремяСтруктура"
              JOIN nodes_branch ON "ОтчетВремяСтруктура"."@Узел" = nodes_branch."Родитель"
           )

           SELECT max(count) FROM nodes_branch;
       '''.format('IS NULL' if not parent_id else '= {}'.format(parent_id))
      local_cur.execute(q)
      return local_cur.fetchone()[0]

   def get_nodes(parent_id):
      q = '''
         WITH person_list AS (
             WITH RECURSIVE tmp("id_dep", "parent") AS (
                 SELECT "@Лицо", "Раздел"
                 FROM "СтруктураПредприятия"
                 WHERE "Раздел" = %(dep_id)s AND "Раздел@" = TRUE
                 UNION
                 SELECT "СП"."@Лицо", "СП"."Раздел"
                 FROM "СтруктураПредприятия" "СП"
                 INNER JOIN tmp
                 ON tmp."id_dep" = "СП"."Раздел"
             )
             SELECT DISTINCT id_dep AS "Лицо"
             FROM tmp
             UNION
             SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
             FROM tmp
             INNER JOIN "СвязиПользователя" "СП"
             ON "СП"."РабочаяГруппа" = tmp."id_dep"
             INNER JOIN "Сотрудник" "С"
                 ON  "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
                 AND ( "С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
                 AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
             UNION
             SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
                 FROM "СвязиПользователя" "СП"
             INNER JOIN "Сотрудник" "С"
                 ON "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
                 AND ("С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
                 AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
             WHERE "СП"."РабочаяГруппа" = %(dep_id)s
         )

           SELECT
               "Родители"[%(level)s],
               SUM("Время"),
               SUM("Количество")
           FROM
               "ОтчетВремяЛицо"
           WHERE
               "Родители"[%(level)s]= ANY(SELECT "@Узел" FROM "ОтчетВремяСтруктура" WHERE "Родитель" {})
               -- AND "Лицо" = ANY(SELECT * FROM  person_list)
           GROUP BY
               "Родители"[%(level)s]
       '''.format('IS NULL' if not parent_id else '= {}'.format(parent_id))
      local_cur.execute(q, {'level': get_level(parent_id),
                            'dep_id': 1371,
                            'parent': 'IS NULL' if not parent_id else '= {}'.format(parent_id)})
      return [f for f in local_cur.fetchall()]

   def print_level(step, nodes):
      for n in nodes:
         print(step, n[0], n[1], n[2])
         print_level(step + ' -> ', get_nodes(n[0]))

   print_level('', get_nodes(None))

get_report()
# process_works()