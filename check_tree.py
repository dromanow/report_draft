import json
import psycopg2

projects = json.loads(open('res.json', 'r').read())
tasks = json.loads(open('tasks.json', 'r').read())

conn = psycopg2.connect(
   database="inside.tensor.ru",
   user="postgres",
   password="postgres",
   host='rc-inside-db2.unix.tensor.ru'
)

cur = conn.cursor()
cur.execute('set local search_path=\'_00000003\';')


class Project:
   def __init__(self, val):
      self.type = "Project"
      self.id = val['id']
      self.stages = list(map(Stage, filter(lambda v: v['type'] == 2059, val.get('stages', []))))
      self.tasks = list(map(Task, filter(lambda v: v['type'] == 15, val.get('stages', []))))

   def __str__(self):
      info = ''
      if len(self.tasks) > 0 or len(self.stages) > 0:
         info = '(tasks: {}, stages {})'.format(len(self.tasks), len(self.stages))
      return self.type + " " + str(self.id) + " " + info

   def _tree_walk(self):
      pass

   def tree_walk(self):
      self._tree_walk()
      for s in self.stages:
         s.tree_walk()
      for t in self.tasks:
         t.tree_walk()

   def get_sub_tasks(self):
      res = list(map(lambda t: t.id, self.tasks))
      for n in self.tasks + self.stages:
         res.extend(n.get_sub_tasks())
      return res


class Stage(Project):
   def __init__(self, val):
      super().__init__(val)
      self._type = "Stage"


class Task(Project):
   def __init__(self, val):
      super().__init__(val)
      self._type = "Task"

   def _tree_walk(self):
      q = '''
      WITH person_list AS (
          WITH RECURSIVE tmp("id_dep", "parent") AS (
              SELECT "@Лицо", "Раздел"
              FROM "СтруктураПредприятия"
              WHERE "Раздел" = 1371 AND "Раздел@" = TRUE
              UNION
              SELECT "СП"."@Лицо", "СП"."Раздел"
              FROM "СтруктураПредприятия" "СП"
              INNER JOIN tmp
              ON tmp."id_dep" = "СП"."Раздел"
          )
          SELECT DISTINCT id_dep AS "Лицо"
          FROM tmp
          UNION
          SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
          FROM tmp
          INNER JOIN "СвязиПользователя" "СП"
          ON "СП"."РабочаяГруппа" = tmp."id_dep"
          INNER JOIN "Сотрудник" "С"
              ON  "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
              AND ( "С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
              AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
          UNION
          SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
              FROM "СвязиПользователя" "СП"
          INNER JOIN "Сотрудник" "С"
              ON "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
              AND ("С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
              AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
          WHERE "СП"."РабочаяГруппа" = 1371
      )

         select
            "РазличныеДокументы"."@Документ"
         from
            "РазличныеДокументы"
         join "Работа" on "РазличныеДокументы"."@Документ" = "Работа"."Документ"
         where
            "БазовыйДокумент" = %(doc_id)s
             AND "Работа"."ЧастноеЛицо" = ANY(SELECT * FROM person_list)
             AND "Работа"."Дата" >= '2015-12-01'::date
             AND "Работа"."Дата" <= '2015-12-31'::date
             AND COALESCE("Работа"."Тип", 0) IN (0, 15)
             AND "Работа"."Удалена" IS NOT TRUE
         group by
            "РазличныеДокументы"."@Документ"
         ;
      '''
      cur.execute(q, {'doc_id': self.id})
      old = len(self.tasks)
      self.tasks.extend(map(lambda v: Task({'id': v[0]}), cur.fetchall()))
      if len(self.tasks) - old:
         print('to {} added {} subtasks'.format(self.id, len(self.tasks) - old))
         if len(self.tasks) - old > 10:
            print(cur.query.decode('cp1251'))


prj = list(map(Project, projects))

for p in prj:
   p.tree_walk()

pass



# subtasks = []

# for p in pobj:
#    subtasks.extend(p.get_sub_tasks())


# q = '''
# SELECT "@Документ", "Регламент"
# FROM "Документ"
# WHERE
# "@Документ" = ANY(%(tasks)s)
# AND "Документ"."Удален" IS NOT TRUE
# ;
# '''
#
# cur.execute(q, {'tasks': subtasks})
#
# regs = {d[0]: d[1] for d in cur.fetchall()}
#
# unlinked_tasks = list(map(lambda t: t['id'], tasks))
# print(unlinked_tasks)
# print(subtasks)

# for t in tasks:


# for p in pobj:

   # p.tree_walk()
