WITH person_list AS (
    WITH RECURSIVE tmp("id_dep", "parent") AS (
        SELECT "@Лицо", "Раздел"
        FROM "СтруктураПредприятия"
        WHERE "Раздел" = {dep_id} AND "Раздел@" = TRUE
        UNION
        SELECT "СП"."@Лицо", "СП"."Раздел"
        FROM "СтруктураПредприятия" "СП"
        INNER JOIN tmp
        ON tmp."id_dep" = "СП"."Раздел"
    )
    SELECT DISTINCT id_dep AS "Лицо"
    FROM tmp
    UNION
    SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
    FROM tmp
    INNER JOIN "СвязиПользователя" "СП"
    ON "СП"."РабочаяГруппа" = tmp."id_dep"
    INNER JOIN "Сотрудник" "С"
        ON  "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
        AND ( "С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
        AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
    UNION
    SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
        FROM "СвязиПользователя" "СП"
    INNER JOIN "Сотрудник" "С"
        ON "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
        AND ("С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
        AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
    WHERE "СП"."РабочаяГруппа" = {dep_id}
)
