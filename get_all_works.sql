WITH person_list AS (
    WITH RECURSIVE tmp("id_dep", "parent") AS (
        SELECT "@Лицо", "Раздел"
        FROM "СтруктураПредприятия"
        WHERE "Раздел" = %(dep_id)s AND "Раздел@" = TRUE
        UNION
        SELECT "СП"."@Лицо", "СП"."Раздел"
        FROM "СтруктураПредприятия" "СП"
        INNER JOIN tmp
        ON tmp."id_dep" = "СП"."Раздел"
    )
    SELECT DISTINCT id_dep AS "Лицо"
    FROM tmp
    UNION
    SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
    FROM tmp
    INNER JOIN "СвязиПользователя" "СП"
    ON "СП"."РабочаяГруппа" = tmp."id_dep"
    INNER JOIN "Сотрудник" "С"
        ON  "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
        AND ( "С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
        AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
    UNION
    SELECT DISTINCT "СП"."ЧастноеЛицо" AS "Лицо"
        FROM "СвязиПользователя" "СП"
    INNER JOIN "Сотрудник" "С"
        ON "СП"."ЧастноеЛицо" = "С"."ЧастноеЛицо"
        AND ("С"."Принят" IS NULL OR "С"."Принят" <= now()::date)
        AND ("С"."Уволен" IS NULL OR "С"."Уволен" >= now()::date)
    WHERE "СП"."РабочаяГруппа" = %(dep_id)s
)

SELECT
   "Работа"."Документ",
   "Работа"."ЧастноеЛицо",
   "Работа"."ВремяКнц"::time - "Работа"."ВремяНач"::time as time,
   "Работа"."ТипДокумента",
   "Работа"."Дата",
   "Документ"."Регламент"
FROM "Работа"
JOIN "Документ" ON "Работа"."Документ" = "Документ"."@Документ"
WHERE
   "Работа"."ЧастноеЛицо" = ANY(SELECT * FROM person_list)
   AND "Работа"."Дата" >= %(start_date)s
   AND "Работа"."Дата" <= %(end_date)s
   AND COALESCE("Работа"."Тип", 0) IN (0, 15)
   AND "Работа"."Удалена" IS NOT TRUE
