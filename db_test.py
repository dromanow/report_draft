from datetime import date, timedelta
import time
import psycopg2


def get_query(cursor):  # decode query text from cursor
   return cursor.query.decode('cp1251')


def get_first_day(dt, d_years=0, d_months=0):  # for month
   y, m = dt.year + d_years, dt.month + d_months
   a, m = divmod(m - 1, 12)
   return date(dt.year + a, m + 1, 1)


def get_last_day(dt):  # for month
   return get_first_day(dt, 0, 1) + timedelta(-1)


def get_sql(name):  # load sql from file
   return open(name + '.sql', 'r').read().encode('cp1251').decode('utf8')


conn = psycopg2.connect(
   database="inside.tensor.ru",
   user="postgres",
   password="postgres",
   host='rc-inside-db2.unix.tensor.ru'
)

cur = conn.cursor()

cur.execute('set local search_path=\'_00000003\';')

users_query = get_sql('person_in_tree').format(dep_id=1371)  # 1371 Разработка

md = date(2015, 12, 1)

cur.execute(users_query + get_sql('get_root_docs'), {
   'start_date': get_first_day(md),
   'end_date': get_last_day(md)
})

res = [{'type_id': d[0],
        'type': d[3],
        'time': d[1].seconds / 60 * 60,
        'count': d[2]}
       for d in cur.fetchall()]

for d in res:
   print(d)

# 2626, 2698 = AND "ПодТипДокумента" IS NULL;
# 2058 = Проект

# print(cur.mogrify(q, {
#    'persons': ids,
#    'start_date': get_first_day(md),
#    'end_date': get_last_day(md)
# }).decode('cp1251'))
# print(get_query(cur))

cur.execute(users_query + get_sql('get_project_list'), {
   'project_type_id': 2058,
   'start_date': get_first_day(md),
   'end_date': get_last_day(md)
})

projects = [{'id': d[0],
             'name': d[3],
             'time': d[1].seconds / 60 * 60,
             'count': d[2]} for d in cur.fetchall()]


for p in projects[:10]:
   start_time = time.time()
   cur.execute(users_query + get_sql('get_stage_list'), {
      'parent_project_id': p['id'],
      'start_date': get_first_day(md),
      'end_date': get_last_day(md)
   })
   stages = [{'id': d[0],
              'name': d[3],
              'time': d[1].seconds / 60 * 60,
              'count': d[2]} for d in cur.fetchall()]
   elapsed_time = time.time() - start_time
   p['stages'] = stages
   p['query_time'] = elapsed_time

pass





# prj_count = 0
# for d in projects:
#    print(d)
#    prj_count += d[2]
# print(prj_count, len(projects))
# print(get_query(cur))

# print(list(map(lambda p: p[0], filter(lambda p: p[1] is None, projects))))

# print(res)


# d[0] if d[0] not in doc_types.keys() or d[0] is None else doc_types[d[0]]

# print(doc_types)
# print(res)

# for d in cur.fetchall():
#    info = {
#       'type': d[0] if d[0] not in doc_types.keys() or d[0] is None else doc_types[d[0]],
#       'time': d[1].hours,
#       'count': d[2]
#    }

# print(cur.query.decode('cp1251'))
#
# for d in cur.fetchall():
#    print(d)
# print(cur.fetchall())
# docs = [r[0] for r in cur.fetchall()]
# print(len(docs))
# print(docs)


